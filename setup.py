#!/usr/bin/env python3.6
# -*- coding: iso-8859-15 -*-
from setuptools import find_packages, setup

from servier.constants import VERSION

setup(
    name='servier',
    version=VERSION,
    author='Sébastien Géneté',
    author_email='sebastien.genete@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['keras', 'numpy', 'tensorflow', 'scikit-learn', 'flask', 'waitress'],
    zip_safe=False,
    entry_points = {
        'console_scripts': ['train=servier.main:train',
                            'evaluate=servier.main:evaluate',
                            'predict=servier.main:predict'],
    },
    python_requires='>=3.8',
)
