
# Drug molecule prediction

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Context

> The prediction of drug molecule properties plays an important role in the drug design process.
> The molecule properties are the cause of failure for 60% of all drugs in the clinical phases.
> A multi parameters optimization using machine learning methods can be used to choose an optimized molecule to be subjected to more extensive studies and to avoid any clinical phase failure.

Drug molecule prediction is a small app to predict drug molecule properties.

# What's new

* [CHANGELOG.md](CHANGELOG.md)

# Tech

## Installation

Drug molecule prediction requires [Miniconda](https://repo.continuum.io/miniconda) to run.

Install the dependencies and devDependencies and start the server.

```sh
make install
```

## API

The API can be started from the root folder with the following command:

```sh
python3 -m servier.server
```

The API is available with you browser at the following adress:

```sh
http://127.0.0.1:5000/predict/<smile>
```

### Example

```sh
http://127.0.0.1:5000/predict/Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C
```

Response:

```sh
"algoVersion":"0.0.1"
"prediction":true
"property":"P1"
"smile":"Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C"
```

## Packaging

This application is installable using a setup.py.

```sh
python3 setup.py install
```

The following commands are available after
installing your package

```sh
servier train <dataset_file> <property> <test_size>
```

or

```sh
servier evaluate <dataset_file> <property>
```

or

```sh
servier predict <smile>
```

**Examples:**

```sh
servier train datasets/dataset_multi.csv P3 0.33
servier evaluate datasets/dataset_multi.csv P3
servier predict "Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C"
```

## Docker

Drug molecule prediction is very easy to install and deploy in a Docker container.

```sh
docker build . -t servier
```

or

```sh
make docker
```

This will create the servier image and pull in the necessary dependencies.

Once done, run the Docker image and map the port to whatever you wish on your host.
In this example, we simply map port 5000 of the host to port 8080 of the Docker:

```sh
docker run -d -p 5000:8080 --restart="always" servier
```

Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:8000
```

## Tests

Unit tests can be launched with:

```sh
make test
```

Lint test can be launched with:

```sh
make lint
```

## Todos

* Write MORE unit tests
* Improve prediction accuracy

License

----

### MIT

## Examples

```sh
python3 -m servier.main train servier/datasets/dataset_single.csv P1 0.33
python3 -m servier.main train servier/datasets/dataset_multi.csv P3 0.33
python3 -m servier.main evaluate servier/datasets/dataset_single.csv P1
python3 -m servier.main evaluate servier/datasets/dataset_multi.csv P3
python3 -m servier.main predict servier/models/dataset_single.csv.P1.model "Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C"
```

# Note

README.md edited on [dillinger](https://dillinger.io/)

# Docker Cheatsheet

$ docker images                     // To view install images
$ docker rmi <IMAGE_NAME>           // To remove an installed image

$ docker ps -a                      // To view all docker containers
$ docker stop <CONTAINER_NAME>      // To stop a docker container
$ docker rm <CONTAINER_NAME>        // To remove a docker container

$ docker exec -it <CONTAINER_NAME> bash    // Execute into container and run bash

If you want to see the log output from a docker container, omit the -d from run.sh

## Docker test

```sh
sebastien@sebastieng-XPS-15-9570:~/git/drug_molecule_predictor$ docker run --name servier_bash --rm -i -t servier bash
servier@397873e47749:~$ python3 -m servier.main train servier/datasets/dataset_single.csv P1 0.33
2021-02-05 23:02:31.571328: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2021-02-05 23:02:31.571358: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
/home/servier
Dataset loading...
Datset loading completed with 4999 molecules!
2021-02-05 23:02:53.652244: I tensorflow/compiler/jit/xla_cpu_device.cc:41] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-02-05 23:02:53.654241: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory
2021-02-05 23:02:53.654262: W tensorflow/stream_executor/cuda/cuda_driver.cc:326] failed call to cuInit: UNKNOWN ERROR (303)
2021-02-05 23:02:53.654283: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:156] kernel driver does not appear to be running on this host (397873e47749): /proc/driver/nvidia/version does not exist
2021-02-05 23:02:53.654497: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-02-05 23:02:53.655360: I tensorflow/compiler/jit/xla_gpu_device.cc:99] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-02-05 23:02:53.856531: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:116] None of the MLIR optimization passes are enabled (registered 2)
2021-02-05 23:02:53.875996: I tensorflow/core/platform/profile_utils/cpu_utils.cc:112] CPU Frequency: 2199995000 Hz
Epoch 1/5
14/14 [==============================] - 0s 4ms/step - loss: 0.0000e+00 - accuracy: 0.8124
Epoch 2/5
14/14 [==============================] - 0s 3ms/step - loss: 0.0000e+00 - accuracy: 0.8235
Epoch 3/5
14/14 [==============================] - 0s 4ms/step - loss: 0.0000e+00 - accuracy: 0.8210
Epoch 4/5
14/14 [==============================] - 0s 4ms/step - loss: 0.0000e+00 - accuracy: 0.8176
Epoch 5/5
14/14 [==============================] - 0s 4ms/step - loss: 0.0000e+00 - accuracy: 0.8151
13/13 [==============================] - 0s 2ms/step - loss: 0.0000e+00 - accuracy: 0.8236
2021-02-05 23:02:54.870451: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
Model save to ./models/dataset_single.csv.P1.model
```

## Model optimization

# Optimizer

<https://towardsdatascience.com/7-tips-to-choose-the-best-optimizer-47bb9c1219e>
Tested optimizers : sgd (chosen) and RMSprop

# Loss function

Loss function tested : categorical_crossentropy and binary_crossentropy

# Activation layer

Activation layer tested :

* 1st layer: standard ReLU activation: max(x, 0), the element-wise maximum of 0 and the input tensor
* last layer: softmax (converts a real vector to a vector of categorical probabilities)
* hidden layer : 0 and 1 (sigmoid/softmax)
