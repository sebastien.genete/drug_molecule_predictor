# ref https://github.com/tebeka/pythonwise/blob/master/docker-miniconda/Dockerfile
FROM ubuntu:18.04

# System packages
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y curl

# Install miniconda to /miniconda (latest miniconda contains python3.8)
RUN curl -LO http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN bash Miniconda3-latest-Linux-x86_64.sh -p /miniconda -b
RUN rm Miniconda3-latest-Linux-x86_64.sh
ENV PATH=/miniconda/bin:${PATH}
RUN conda update -y conda

# Python packages from conda
RUN	conda create -y --name servier python=3.8
RUN	conda install -y -c conda-forge rdkit

# Define user to avoid the use of root user
RUN addgroup --system servier
RUN adduser --system --disabled-password --home /home/servier --group servier
WORKDIR /home/servier
COPY --chown=servier:servier ./ ./
RUN pip install -r requirements.txt

EXPOSE 5000
USER servier
CMD ["/miniconda/bin/python3", "-m servier.server"]