'''
    API server
'''
from flask import Flask
from flask import jsonify, make_response
from waitress import serve

from servier.main import DrugMoleculePredictor
from servier.constants import VERSION

app = Flask(__name__)

# TODO: add more monitoring
@app.route('/health', methods=['GET'])
def health():
    '''
        Route to monitor the api health
    '''
    return 200

@app.route('/predict/<smile>', methods=['GET'])
def predict(smile):
    '''
        Predict the drug molecule property P1 from a smile string
    '''
    try:
        dmp = DrugMoleculePredictor()
        # Load model
        model_file = './models/dataset_single.csv.P1.model'
        model = dmp.load_dl_model(model_file)
        # Compute prediction
        prediction = dmp.predict(model, smile)
        data = {'property': 'P1', 'prediction': bool(prediction), 'smile': smile, "algoVersion": VERSION}
        return make_response(jsonify(data), 200)
    except Exception as exception:
        data = {'property': 'P1', 'prediction': None, 'smile': smile, "algoVersion": VERSION,
                'error': str(exception) }
        return make_response(jsonify(data), 400)

if __name__ == '__main__':
    serve(app, host='127.0.0.1', port=5000)

# TODO: Load the model(s) before the query
# TODO: Accept queries on different properties
