''' Some utils like an features exatractor'''
from rdkit.Chem import rdMolDescriptors, MolFromSmiles, rdmolfiles, rdmolops

def fingerprint_features(smile_string: str, radius=2, size: int=2048):
    '''
        Extract features from a smile string
    '''
    mol = MolFromSmiles(smile_string)
    new_order = rdmolfiles.CanonicalRankAtoms(mol)
    mol = rdmolops.RenumberAtoms(mol, new_order)
    return rdMolDescriptors.GetMorganFingerprintAsBitVect(mol, radius,
                                                          nBits=size,
                                                          useChirality=True,
                                                          useBondTypes=True,
                                                          useFeatures=False
                                                          )
