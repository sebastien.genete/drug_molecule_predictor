''' Module to train, evaluate and predict drug properties'''
import sys
from os import path
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from keras.models import Sequential, load_model
from keras.layers import Dense

from servier.utils.feature_extractor import fingerprint_features


class DrugMoleculePredictor():
    '''Class to train, evaluate and predict drug properties
    '''

    def __init__(self):
        pass

    def load_data_set(self, dataset_file: str):
        '''Method used to load a data set

        Parameters
        ----------
        dataset_file : str
            The relative path to the dataset file. Accepted file format: csv

        Returns
        -------
        dataset_features
            A pandas DataFrame with binary features
        dataset_output
            A pandas DataFrame with ref property

        '''
        # dataset loading
        print('Dataset loading...')
        dataset = pd.read_csv(dataset_file)
        if 'smiles' in dataset.columns:
            # features extraction
            dataset_features =  pd.DataFrame([list(fingerprint_features(smile)) for smile in dataset['smiles']])
            # dataset output
            dataset_output = dataset.drop(columns = 'smiles', inplace = False)
            print(f'Datset loading completed with {len(dataset_features)} molecules!')
            return dataset_features, dataset_output
        else:
            raise ValueError('No smiles found in the dataset.')

    def train_model(self, dataset_file: str, selected_property: str, test_size: float=0.0):
        ''' Method to train a model, to predict the drug property

        Parameters
        ----------
        dataset_file : str
            The relative path to the dataset file. Accepted file format: csv
        selected_property: str
            The drug molecule property P1,...P9
        test_size: float
            The proportion of samples to test the model.
            Must be included in the range 0.0-1.0

        Returns
        -------
        model_name: str
            The relative path to the model name.

        '''
        # Load data
        dataset_features, dataset_output = self.load_data_set(dataset_file)
        # Init model
        model = self.init_model()
        # Prepare data
        X = dataset_features.to_numpy()
        if selected_property in dataset_output.columns:
            Y = dataset_output[selected_property].to_numpy()
        else:
            raise ValueError(f'Property {selected_property} not found in the dataset.')
        # Fix random seed for reproducibility
        seed = 0
        np.random.seed(0)
        # Eval
        eval_required = (test_size>0 and test_size<1)
        # split the dataset in training/test dataset
        if eval_required:
            X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size,
                                                                random_state=seed)
        else:
            X_train, X_test, Y_train, Y_test = X, None, Y, None
        # Model training
        model.fit(X_train, Y_train, epochs=5, batch_size=256)
        # Model evaluation
        if eval_required:
            model.evaluate(X_test, Y_test, batch_size=128)
        # Save model
        _, file_name = path.split(dataset_file)
        model_name = f'./models/{file_name}.{selected_property}.model'
        model.save(model_name)
        print(f'Model save to {model_name}')
        return model_name

    def load_dl_model(self, model_file: str):
        ''' Method to load a deep learning model

        Parameters
        ----------
        model_file: str
            The relative path to the model name.

        Returns
        -------
        model: tensorflow.python.keras.engine.sequential.Sequential
            A deep learning model.

        '''
        print(f'Model {model_file} loading...')
        if not path.exists(model_file):
            raise FileNotFoundError
        else:
            model = load_model(model_file)
            print(f'Model {model_file} loaded.')
        return model

    def evaluate_model(self, model, dataset_features: pd.DataFrame, dataset_output: pd.DataFrame,
                       selected_property: str):
        ''' Method to evaluate a model of drug property prediction

        Parameters
        ----------
        dataset_features
            A pandas DataFrame with binary features
        dataset_output
            A pandas DataFrame with ref property
        selected_property: str
            The drug molecule property P1,...P9

        Returns
        -------
        loss_and_metrics: dict
            A dict with keys:
                loss
                accuracy

        '''
        X = dataset_features.to_numpy()
        if selected_property in dataset_output.columns:
            Y = dataset_output[selected_property].to_numpy()
        loss_and_metrics = model.evaluate(X, Y, batch_size=128)
        return {'loss': loss_and_metrics[0], 'accuracy': loss_and_metrics[1]}

    def init_model(self):
        ''' Method to init a deep learning model

        Parameters
        ----------

        Returns
        -------
        model: tensorflow.python.keras.engine.sequential.Sequential
            A deep learning model.

        '''
        # DL model setup
        model = Sequential()
        model.add(Dense(units=64, activation='relu', input_dim=2048))
        model.add(Dense(units=1, activation='softmax'))
        model.compile(loss='categorical_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
        return model

    def predict(self, model, smile: str)->float:
        ''' Method to predict a drug property

        Parameters
        ----------
        model: tensorflow.python.keras.engine.sequential.Sequential
            A deep learning model.
        smile: str
            A string representation for a molecule

        Returns
        -------
        prediction: float(bool)
            The prediction for the smile with the model

        '''
        prediction = None
        if smile:
            try:
                features = np.array([fingerprint_features(smile)])
                prediction = model.predict(features)[0][0]
            except AttributeError:
                print('An error occurred...')
        return prediction


def train(dataset_file: str, selected_property: str, test_size:float)->str:
    '''Function to train a model, to predict the drug property

        Parameters
        ----------
        dataset_file : str
            The relative path to the dataset file. Accepted file format: csv
        selected_property: str
            The drug molecule property P1,...P9
        test_size: float
            The proportion of samples to test the model.
            Must be included in the range 0.0-1.0

        Returns
        -------
        model_name: str
            The relative path to the model name.

    '''
    dmp = DrugMoleculePredictor()
    model_name = dmp.train_model(dataset_file, selected_property, test_size)
    return model_name

def evaluate(dataset_file: str, selected_property: str):
    ''' Function to evaluate a model of drug property prediction

        Parameters
        ----------
        dataset_file : str
            The relative path to the dataset file. Accepted file format: csv
        selected_property: str
            The drug molecule property P1,...P9

        Returns
        -------
        loss_and_metrics: dict
            A dict with keys:
                loss
                accuracy

        '''
    dmp = DrugMoleculePredictor()
    _, file_name = path.split(dataset_file)
    model_file = f"./servier/models/{file_name}.{selected_property}.model"
    model = dmp.load_dl_model(model_file)
    # Load data set
    dataset_features, dataset_output = dmp.load_data_set(dataset_file)
    # Eval model with data set
    return dmp.evaluate_model(model, dataset_features, dataset_output, selected_property)

def predict(model_file: str, smile: str):
    ''' Function to predict a drug property prediction of the smile with the model file

        Parameters
        ----------
        model_file: str
            The relative path to the deep learning model.
        smile: str
            A string representation for a molecule

        Returns
        -------
        prediction: float(bool)
            The prediction for the smile with the model

        '''
    dmp = DrugMoleculePredictor()
    # Load model
    model = dmp.load_dl_model(model_file)
    # Compute prediction
    prediction = dmp.predict(model, smile)
    print(f'Predicted property: {prediction}')
    return prediction

if __name__ == '__main__':
    action = sys.argv[1]
    if action == 'train':
        # Train a model from a dataset to predict a property
        dataset_file = sys.argv[2] # Relative path to the csv file
        selected_property = sys.argv[3]  # 'P1'
        test_size = float(sys.argv[4])  # 0.33
        train(dataset_file, selected_property, test_size)
    elif action == 'evaluate':
        # Evaluate a model build from a dataset for a property
        dataset_file = sys.argv[2] # Relative path to the csv file
        selected_property = sys.argv[3]  # 'P1'
        evaluate(dataset_file, selected_property)
    elif action == 'predict':
        # Compute prediction
        model_file = sys.argv[2] # Relative path to the model file
        smile = sys.argv[3]
        prediction = predict(model_file, smile)
        sys.exit(prediction)
