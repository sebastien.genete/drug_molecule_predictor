''' unit test: main module '''
import unittest

#from servier.utils.feature_extractor import fingerprint_features
from servier.main import DrugMoleculePredictor

class DrugMoleculePredictorTest(unittest.TestCase):
    ''' Class for testing the drug molecule predictor class'''
    def setUp(self):

        self.model = '....'
        self.dataset_file = 'servier/datasets/dataset_single.csv'
        self.dmp = DrugMoleculePredictor()
        self.model_file = self.dmp.train_model(self.dataset_file, 'P1', 0.0)
        self.model = self.dmp.load_dl_model(self.model_file)

    def test_train(self):
        ''' method to test the train function'''

        self.assertEqual(self.model_file, './models/dataset_single.csv.P1.model')


    def test_predict(self):
        ''' method to test the predict function'''

        smile = 'Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C'
        ref_value = 1.0

        # Compute prediction with valid model and valid smile

        prediction = self.dmp.predict(self.model, smile)
        self.assertEqual( prediction, ref_value)

        # Compute prediction with unvalid model and valid smile
        prediction = self.dmp.predict(None, smile)
        self.assertEqual(prediction, None)

        # Compute prediction with unvalid model and unvalid smile
        prediction = self.dmp.predict(None, '')
        self.assertEqual(prediction, None)

        # Compute prediction with valid model and unvalid smile
        prediction = self.dmp.predict(self.model, '')
        self.assertEqual(prediction, None)

    def test_evaluate(self):
        ''' method to test the evaluate function'''

        # Load data set
        dataset_features, dataset_output = self.dmp.load_data_set(self.dataset_file)
        # Eval model with data set
        eval_res = self.dmp.evaluate_model(self.model, dataset_features, dataset_output, 'P1')

        self.assertEqual(eval_res, {'loss':0.0,'accuracy': 0.821764349937439})
