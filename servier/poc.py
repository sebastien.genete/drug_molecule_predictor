import sys
from os import path
import pandas as pd
import numpy as np
from typing import List
from sklearn.model_selection import train_test_split
from keras.models import Sequential, load_model
from keras.layers import Dense

from servier.utils.feature_extractor import fingerprint_features



dataset_file1 = 'servier/datasets/dataset_single.csv'
dataset = pd.read_csv(dataset_file1)

# features extraction

# dataset['features'] = dataset['smiles'].apply(fingerprint_features)
dataset_features = [list(fingerprint_features(smile)) for smile in dataset['smiles']]
dataset_features = pd.DataFrame(dataset_features)

print('Features extraction done!')

# features number: 2048
# nb_features = len(dataset.iloc[0]['features'])

# 1st model
model = Sequential()
model.add(Dense(units=64, activation='relu', input_dim=2048))
model.add(Dense(units=1, activation='softmax'))
model.compile(loss='categorical_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])

# train
X = dataset_features.to_numpy()
Y = dataset['P1'].to_numpy()
# fix random seed for reproducibility
seed = 7
np.random.seed(seed)
# split into 67% for train and 33% for test
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=seed)

# Model training
model.fit(X_train, Y_train, epochs=5, batch_size=256)
# Model evaluation
loss_and_metrics = model.evaluate(X_test, Y_test, batch_size=128)


smile = 'Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C'




