PIP=pip3
PYTHON=python3.8

docker:
	docker --debug build . -t servier

# Installation & development
deps:
		$(PIP) install -r requirements.txt

install:
	wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
	chmod u+x Miniconda3-latest-Linux-x86_64.sh
	./Miniconda3-latest-Linux-x86_64.sh -b -p ~/miniconda -u
	rm -f Miniconda3-latest-Linux-x86_64.sh
	export PATH=~/miniconda/bin:$PATH

	conda update -y -n base conda
	conda create -y --name servier python=3.8
	conda activate servier
	conda install -y -c conda-forge rdkit # Note. Impossible to install rdkit directly with pip
	$(PIP) install -r requirements.txt

test:
	$(PYTHON) -m unittest discover servier/tests test*.py

coverage:
	coverage erase
	coverage run -a -m unittest discover servier/tests test*.py
	coverage report -i -m

#lint:
#    pycodestyle