# CHANGELOG

## 0.0.1

### Common

* first release

### Features

* Train the model
* Evaluate the model
* Predict a property P1...P9 for any given smile
* A simple flask api to serve you model with just one route /predict that you can use to send your molecule smile and get the prediction
